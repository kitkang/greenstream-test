<import resource="classpath:/alfresco/templates/org/alfresco/import/alfresco-util.js">

function main()
{
   AlfrescoUtil.param('nodeRef');
   AlfrescoUtil.param('site', null);
   var documentDetails = AlfrescoUtil.getNodeDetails(model.nodeRef, model.site);
   if (documentDetails)
   {
      model.title = documentDetails.item.node.properties["cm:title"] || "";
	  model.folder = documentDetails.item.node.properties["erp:reference_documents"] || "";
   }
}

main();

model.widgets = [];
