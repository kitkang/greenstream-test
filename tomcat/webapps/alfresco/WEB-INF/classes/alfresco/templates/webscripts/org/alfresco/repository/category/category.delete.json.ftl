{
    <#if redirect??>"redirect": "${redirect}",</#if>
    <#if persistedObject??>"persistedObject": "${persistedObject?string}",</#if>
    <#if name??>"name": "${name}",</#if>
    "message": "${message}"
}